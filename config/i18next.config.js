/**
 * Internationalization
 */

const i18next = require('i18next')
const { join } = require('path')
const i18nBackend = require('i18next-fs-backend')
const languageDetector = require('i18next-browser-languagedetector')

i18next.use(languageDetector).use(i18nBackend).init({
    fallbackLng: 'en',
    // debug: true, /* disable on production */
    defaultNS: 'infos',
    ns: ['errors', 'infos'],
    backend: {
        loadPath: join(__dirname, '../locales/{{lng}}/{{ns}}.json'),
    },
})

module.exports = i18next

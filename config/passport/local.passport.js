const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const bcrypt = require('bcrypt')

const User = require('@auth/models/user.model')

passport.use(new LocalStrategy(
    {
        usernameField: 'username',
        passwordField: 'password',
    },
    (username, password, done) => {
        User.findOne({ username }, 'userId firstname lastname username password').exec((err, user) => {
            if (err) {
                console.log(err)
                return done(err)
            }
            if (!user) {
                console.log('passport user not found')
                return done(null, false)
            }
            bcrypt.compare(password, user.password, (bcrypterr, result) => {
                if (bcrypterr) {
                    return done(null, false)
                }
                if (!result) {
                    console.log('passport password wrong')
                    return done(null, {
                        wrongPwd: true,
                    }, user)
                }
                return done(null, user)
            })
        })
    },
))

module.exports = passport

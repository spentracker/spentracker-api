const mongoose = require('mongoose')
const i18next = require('./i18next.config')
const env = require('../env')

const db = {
    dbname: env.mongodb.database,
    username: env.mongodb.username,
    pwd: env.mongodb.password,
    host: env.mongodb.host,
    port: env.mongodb.port,
}

const dbOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    serverSelectionTimeoutMS: env.mongodb.serverSelectionTimeout,
    socketTimeoutMS: env.mongodb.socketTimeout,
}

const dbUrl = `mongodb://${db.username}:${db.pwd}@${db.host}:${db.port}/${db.dbname}`

mongoose.connect(dbUrl, dbOptions).then(() => {
    console.log(i18next.t('database_connected'))
}).catch((error) => {
    console.log(i18next.t('errors:database_conn_err'), error)
})

module.exports = mongoose

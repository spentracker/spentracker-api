require('module-alias/register')
const app = require('@root/app')
const i18next = require('@config/i18next.config')
const srvConfig = require('@root/env').server
require('@config/database.config')

app.listen(srvConfig.port, (err) => {
    if (err) {
        console.error(i18next.t('errors:init_server_failure'))
        console.error(err)
    }

    /** exception from i18n */
    console.log(`server is open and running on http://${srvConfig.host}:${srvConfig.port}/`)
})

app.get('/', (req, res) => {
    res.status(200).json({
        message: i18next.t('server_open'),
    })
})

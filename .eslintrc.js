module.exports = {
    env: {
        browser: true,
        es2021: true,
    },
    extends: 'airbnb-base',
    overrides: [
    ],
    parserOptions: {
        ecmaVersion: 'latest',
        sourceType: 'module',
    },
    rules: {
        semi: ['error', 'never'],
        indent: ['warn', 4],
        'no-console': 0,
        'import/no-unresolved': 0,
        'import/extensions': 0,
        'consistent-return': 0,
        'linebreak-style': 0,
    },
}

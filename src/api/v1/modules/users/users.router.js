/**
 *
 * Routes related to
 * 1. Get Profile
 * 2. Edit Profile
 *
 */
const express = require('express')

const router = express.Router()
const usersCtrl = require('@users/controllers/profile.controller')

router.get('/me', usersCtrl.getProfile)

module.exports = router

/**
 *
 * Services related to:
 * 1. Get Profile
 * 2. Edit Profile
 *
 */
const User = require('@auth/models/user.model')

exports.fetchByUsername = (username, callback) => {
    User.findOne({ username }, (err, user) => {
        if (err) {
            return callback({
                error: 'Something went wrong!',
            })
        }
        if (!user) {
            return callback({
                error: 'User not found',
            })
        }

        return callback(null, user)
    })
}

/**
 *
 * Controller with services related to:
 * 1. User Profile
 * 2. Edit Profile
 *
 */
const UserService = require('@users/services/users.service')
const status = require('@config/http.status')

/**
 * @api {get} /v1/users/me Get active user profile
 * @apiName GetProfile
 * @apiGroup Users
 *
 * @apiHeader Authorization Bearer accessToken
 *
 * @apiSuccess {String} userId
 * @apiSuccess {String} firstname
 * @apiSuccess {String} lastname
 * @apiSuccess {String} username
 * 
 */

exports.getProfile = (req, res) => {
    const { user } = req
    UserService.fetchByUsername(user.username, (err, userDoc) => {
        if (err) {
            return res
                .status(status.notFound)
                .json(err)
        }

        const {_id, __v, ...userData} = userDoc.toJSON()

        return res
            .status(status.ok)
            .json(userData)
    })
}

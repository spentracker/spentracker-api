const Joi = require('joi')

const schema = Joi.object({
    firstname: Joi
        .string()
        .min(2)
        .required(),
    lastname: Joi
        .string()
        .min(2),
    username: Joi
        .string()
        .alphanum()
        .min(4)
        .max(20)
        .required(),
    password: Joi
        .string()
        .alphanum()
        .min(8)
        .max(20)
        .required(),
    cPassword: Joi
        .any()
        .valid(Joi.ref('password'))
        .required(),
})

module.exports = schema

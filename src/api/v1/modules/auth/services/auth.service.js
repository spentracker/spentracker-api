/**
 *
 * Services related to:
 * 1. Login
 * 2. Logout
 * 3. Registration
 *
 */

const User = require('@auth/models/user.model')

exports.addUser = (payload, callback) => {
    const user = new User(payload)
    user.save((err, doc) => {
        if (err) return callback(err)

        const { __v, _id, password, ...pubFields } = JSON.parse(JSON.stringify(doc))
        return callback(null, pubFields)
    })
}

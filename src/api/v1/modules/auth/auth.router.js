/**
 *
 * Routes related to
 * 1. Login
 * 2. Logout
 * 3. Registration
 *
 */
const express = require('express')

const router = express.Router()
const authCtrl = require('@auth/controllers/auth.controller')

router.post('/local/signup', authCtrl.signup)
router.post('/local/login', authCtrl.login)

module.exports = router

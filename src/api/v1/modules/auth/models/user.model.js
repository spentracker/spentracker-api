const mongoose = require('mongoose')
const cuid = require('cuid')

const UserSchema = mongoose.Schema({
    userId: {
        type: String,
        index: true,
        unique: true,
        required: true,
        default: cuid(),
    },
    firstname: {
        type: String,
        required: true,
    },
    lastname: {
        type: String,
        required: true,
    },
    username: {
        type: String,
        index: true,
        unique: true,
        required: true,
    },
    password: {
        type: String,
        select: false,
        required: true,
    },
})

module.exports = mongoose.model('User', UserSchema)

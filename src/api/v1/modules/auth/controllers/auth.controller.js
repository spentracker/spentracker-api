/**
 *
 * Controller with services related to:
 * 1. Login
 * 2. Logout
 * 3. Registration: signup
 *
 */

const bcrypt = require('bcrypt')
const status = require('@config/http.status')
const env = require('@root/env')
const passport = require('passport')
const jwtHelper = require('@helpers/jwt.helper')
const i18next = require('@config/i18next.config')
const UserService = require('@auth/services/auth.service')
const signupSchema = require('@auth/validations/signup.schema')

/**
 * @api {post} /v1/auth/local/signup User Registration
 * @apiName UserRegistration
 * @apiGroup Users
 *
 * @apiParam {String} firstname Firstname of User
 * @apiParam {String} lastname Lastname of User
 * @apiParam {String} username Username to use while logging in
 * @apiParam {String} password Password
 * @apiParam {String} cPassword Repeat Password
 *
 * @apiSuccess {String} message New user registered successfully.
 * @apiSuccess {Object} data Object with registered info
 * 
 */

exports.signup = (req, res) => {
    const user = req.body

    /** data validation */
    const validation = signupSchema.validate(user, { abortEarly: false })
    if (validation.error) {
        return res.status(status.badRequest)
            .json({
                error: validation.error.details,
            })
    }

    bcrypt.genSalt(env.bcrypt.saltRounds, (saltErr, salt) => {
        if (saltErr) {
            return res.status(status.serverErr).json({
                message: saltErr,
            })
        }

        bcrypt.hash(user.password, salt, (hashErr, hash) => {
            if (hashErr) {
                return res.status(status.badRequest).json({
                    error: hashErr,
                })
            }

            user.password = hash
            delete user.confirmPassword

            UserService.addUser(user, (err, doc) => {
                if (err) {
                    if (err.code === 11000) {
                        return res.status(status.badRequest).json({
                            error: i18next.t('errors:existing_user'),
                        })
                    }

                    return res.status(status.serverErr).json({
                        error: err,
                    })
                }

                return res.status(status.created).json({
                    message: i18next.t('user_created'),
                    data: doc
                })
            })
        })
    })
}

/**
 * @api {post} /v1/auth/local/login User Login
 * @apiName UserLogin
 * @apiGroup Users
 *
 * @apiParam {String} username
 * @apiParam {String} password
 *
 * @apiSuccess {String} accessToken
 * @apiSuccess {String} refreshToken
 * 
*/

exports.login = (req, res, next) => {
    passport.authenticate('local', (err, user) => {
        if (err) return next(err)

        if (!user) {
            return res.status(status.notFound).json({
                error: {
                    message: i18next.t('errors:no_user'),
                },
            })
        }

        if (user.wrongPwd) {
            return res.status(status.unauthorized).json({
                error: {
                    message: i18next.t('errors:wrong_pwd'),
                },
            })
        }

        const {_id, password, ...userInfo} = JSON.parse(JSON.stringify(user))
        jwtHelper.generateATRT(userInfo, (tokenErr, tokens) => {
            if (tokenErr) return res.status(status.unauthorized).json(tokenErr)

            return res
                .status(status.ok)
                .cookie('accessToken', tokens.accessToken)
                .cookie('refreshToken', tokens.refreshToken)
                .json(tokens)
        })
    })(req, res, next)
}

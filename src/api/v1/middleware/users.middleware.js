const jwt = require('jsonwebtoken')
const status = require('@config/http.status')
const env = require('@root/env')

exports.authenticate = (req, res, next) => {
    if (req.headers.authorization) {
        const authHeader = req.headers.authorization
        const token = authHeader.split(' ')[1]

        jwt.verify(token, env.jwt.accessTokenSecret, (err, user) => {
            if (err) {
                if (err.name === 'TokenExpiredError') {
                    return res.status(status.unauthorized).json({
                        error: 'Your token has been expired.',
                    })
                }
                return res.status(status.unauthorized).json({
                    error: err,
                })
            }
            req.user = user
            next()
        })
    } else {
        return res.status(status.unauthorized).json({
            error: 'No token available for authentication.',
        })
    }
}

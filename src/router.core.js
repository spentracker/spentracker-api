/**
 * Import all routers from all modules here
 */
const auth = require('@auth/auth.router')
const users = require('@users/users.router')

// v1
const router = {
    authRouter: auth,
    usersRouter: users,
}

module.exports = router

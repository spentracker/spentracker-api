module.exports = {
    server: {
        host: '',
        port: ''
    },
    mongodb: {
        host: '',
        port: '',
        username: '',
        password: '',
        database: '',
        serverSelectionTimeout: 5000,
        socketTimeout: 45000
    },
    bcrypt: {
        saltRounds: 10
    },
    jwt: { /** require('crypto').randomBytes(64).toString('hex') */
        accessTokenSecret: '',
        refreshTokenSecret: '',
        accessTokenExpiresIn: '',
        refreshTokenExpiresIn: '',
        algorithm: ['']
    },
    pagination: {
        hits: 5
    }
}

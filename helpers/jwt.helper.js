const jwt = require('jsonwebtoken')
const env = require('@root/env')

const jwtConfig = env.jwt

exports.generateATRT = (user, callback) => {
    const ATOptions = {
        expiresIn: jwtConfig.accessTokenExpiresIn,
    }

    const RTOptions = {
        expiresIn: jwtConfig.refreshTokenExpiresIn,
    }

    jwt.sign(
        user,
        jwtConfig.accessTokenSecret,
        ATOptions,
        (ATErr, accessToken) => {
            if (ATErr) return callback(ATErr)

            jwt.sign(
                user,
                jwtConfig.refreshTokenSecret,
                RTOptions,
                (RTErr, refreshToken) => {
                    if (RTErr) return callback(RTErr)

                    return callback(null, {
                        accessToken,
                        refreshToken,
                    })
                },
            )
        },
    )
}

/** verifies token and extracts user */
exports.extractUser = (token, callback) => {
    jwt.verify(token, env.jwt.accessTokenSecret, (err, user) => {
        if (err) {
            if (err.name === 'TokenExpiredError') {
                return callback({
                    error: {
                        message: 'Your token has been expired.',
                    },
                })
            }
            return callback({
                error: {
                    message: 'Something went wrong during authentication. Please try again.',
                },
            })
        }

        return callback(null, user)
    })
}

const express = require('express')
const cors = require('cors')
const localPassport = require('@config/passport/local.passport')
const middleware = require('./src/api/v1/middleware/users.middleware')
const router = require('./src/router.core')

const app = express()

app.use(localPassport.initialize())
app.use(cors())
app.use(express.json())

/** v1.0 */
app.use('/v1/auth', router.authRouter)
app.use('/v1/users', middleware.authenticate, router.usersRouter)

module.exports = app
